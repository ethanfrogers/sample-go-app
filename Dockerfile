FROM golang:1.10-alpine as builder

ENV PKG=/go/src/gitlab.com/ethanfrogers/sample-go-app

ADD . $PKG
WORKDIR $PKG

ENV CGO_ENABLED 0

RUN go install -v

FROM alpine:3.6
EXPOSE 3000

RUN apk add -U ca-certificates

COPY --from=builder /go/bin/sample-go-app /app/sample-go-app
COPY templates /app/templates
WORKDIR /app

CMD ["./sample-go-app"]
