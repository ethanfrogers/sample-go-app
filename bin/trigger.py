import subprocess
import sys
import requests
import json
import os
import urllib

SPINNAKER_API = os.environ['SPINNAKER_API']
GIT_SHA=os.environ['CI_COMMIT_SHA']
GITLAB_REF_TEMPLATE = 'https://gitlab.com/api/v4/projects/ethanfrogers%2Fsample-go-app/repository/files/{}/raw'

def check_if_diff(path):
    result = subprocess.run(['git', '--no-pager','diff', '--name-only', '--diff-filter=AM', 'HEAD~1..HEAD', path], stdout=subprocess.PIPE)
    diff = result.stdout.decode('utf-8')
    return diff.strip()

with open('app.json') as f:
    app_meta = json.load(f)

artifacts = []
# if we've made any changes in our Dockerfile or or source, we should trigger a deployment
if check_if_diff('Dockerfile') != '' or check_if_diff('templates/') != '' or check_if_diff('main.go') != '' or check_if_diff('app.json'):
    print('detected change for docker image, adding to artifacts')
    artifacts.append({
        'type': 'docker/image',
        'name': 'registry.gitlab.com/ethanfrogers/sample-go-app',
        'reference': 'registry.gitlab.com/ethanfrogers/sample-go-app:{}'.format(app_meta['version']),
        'version': app_meta['version']
    })

manifest_diff=check_if_diff('manifests/')
if manifest_diff != '':
    print('detected changes to kubernetes manifests, adding to artifacts')
    files=manifest_diff.split("\n")
    for f in files:
        artifacts.append({
            'type': 'gitlab/file',
            'name': f,
            'reference': GITLAB_REF_TEMPLATE.format(urllib.parse.quote_plus(f)),
            'version': GIT_SHA
        })

if len(artifacts) == 0:
    print('no need to trigger a deployment.')
    sys.exit()


headers = {'Content-Type': 'application/json'}
data = {'artifacts': artifacts}

print('triggering pipeline with the following artifacts:')
print(data)

r = requests.post("{}/webhooks/webhook/gitlab".format(SPINNAKER_API),
    data=json.dumps(data),
    headers=headers)

print(r.status_code)
print(r.text)
