#!/bin/bash

apk add -U jq
version=$(cat app.json | jq -r '.version')

docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
docker build -t registry.gitlab.com/ethanfrogers/sample-go-app:$version .
docker push registry.gitlab.com/ethanfrogers/sample-go-app:$version