package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
)

type Settings struct {
	Maintenance bool
}

var appSettings = Settings{}

func init() {
	maint := os.Getenv("MAINTENANCE_MODE")
	if maint == "true" {
		appSettings.Maintenance = true
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	f, _ := ioutil.ReadFile("templates/index.html")
	tmpl, _ := template.New("index").Parse(string(f))

	tmpl.Execute(w, appSettings)
}

func main() {
	http.HandleFunc("/", handler)

	fmt.Println("starting server on 8080")
	if err := http.ListenAndServe(":8080", nil); err != http.ErrServerClosed {
		fmt.Println(err.Error())
	}
}
